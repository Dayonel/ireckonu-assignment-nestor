﻿using Newtonsoft.Json;

namespace FilesAPI.Core.DTOs
{
    public class FileLineDTO
    {
        [JsonIgnore]
        public string FilePath { get; set; }
        [JsonIgnore]
        public string LineText { get; set; }
        [JsonIgnore]
        public bool IsLastLine { get; set; }

        public void SetFileLine(string filePath, string lineText, bool isLastLine)
        {
            FilePath = filePath;
            LineText = lineText;
            IsLastLine = isLastLine;
        }
    }
}
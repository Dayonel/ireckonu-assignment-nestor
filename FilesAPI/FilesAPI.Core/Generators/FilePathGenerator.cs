﻿using FilesAPI.Core.Interfaces.Generators;
using System;

namespace FilesAPI.Core.Generators
{
    public class FilePathGenerator : IFilePathGenerator
    {
        public string GenerateFilePath()
        {
            return $"{PathConstants.UPLOADED_FILES_FOLDER}/{Guid.NewGuid().ToString()}{JSONConstants.JSON_EXTENSION}";
        }
    }
}
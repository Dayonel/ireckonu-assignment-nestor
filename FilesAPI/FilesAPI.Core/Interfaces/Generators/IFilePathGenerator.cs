﻿namespace FilesAPI.Core.Interfaces.Generators
{
    public interface IFilePathGenerator
    {
        string GenerateFilePath();
    }
}
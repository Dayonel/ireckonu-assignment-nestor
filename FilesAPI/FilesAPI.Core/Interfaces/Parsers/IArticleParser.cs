﻿using FilesAPI.Core.DTOs;

namespace FilesAPI.Core.Interfaces.Parsers
{
    public interface IArticleParser
    {
        ArticleDTO Parse(string line);
    }
}
﻿using System.Threading.Tasks;

namespace FilesAPI.Core.Interfaces.PhysicalStorage
{
    public interface IPhysicalArticleStorage
    {
        Task<bool> Write(string filePath, string text);
        Task<bool> WriteLine(string filePath, string line);
    }
}
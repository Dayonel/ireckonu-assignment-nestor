﻿using FilesAPI.Core.Models;
using System.Threading.Tasks;

namespace FilesAPI.Core.Interfaces.Repositories
{
    public interface IArticleRepository
    {
        Task<bool> AddArticle(Article article);
    }
}
﻿using FilesAPI.Core.DTOs;
using System.Threading.Tasks;

namespace FilesAPI.Core.Interfaces.Services
{
    public interface IDataStoreService
    {
        Task<bool> AddArticle(ArticleDTO articleDTO);
    }
}
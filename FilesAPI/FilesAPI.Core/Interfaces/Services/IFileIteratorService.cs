﻿using FilesAPI.Core.DTOs;
using System.Collections.Generic;
using System.IO;

namespace FilesAPI.Core.Interfaces.Services
{
    public interface IFileIteratorService
    {
        IAsyncEnumerable<FileLineDTO> IterateLines(Stream stream, bool skipFirstLine = false);
    }
}
﻿using System.IO;
using System.Threading.Tasks;

namespace FilesAPI.Core.Interfaces.Services
{
    public interface IFileService
    {
        Task<bool> UploadFile(Stream stream);
    }
}
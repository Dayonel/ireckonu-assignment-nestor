﻿using FilesAPI.Core.Interfaces.PhysicalStorage;

namespace FilesAPI.Core.Interfaces.Services
{
    public interface IJSONDataStoreService : IDataStoreService, IPhysicalArticleStorage
    {
    }
}
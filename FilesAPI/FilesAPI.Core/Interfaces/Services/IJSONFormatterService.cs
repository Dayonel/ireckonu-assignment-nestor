﻿using System.Threading.Tasks;

namespace FilesAPI.Core.Interfaces.Services
{
    public interface IJSONFormatterService
    {
        string SerializeToJSON<T>(T input);
    }
}
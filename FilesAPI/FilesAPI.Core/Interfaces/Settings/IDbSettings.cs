﻿namespace FilesAPI.Core.Interfaces.Settings
{
    public interface IDbSettings
    {
        string ConnectionString { get; set; }
    }
}
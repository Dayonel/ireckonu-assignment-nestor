﻿namespace FilesAPI.Core
{
    public static class JSONConstants
    {
        public const string JSON_SEPARATOR = ",";
        public const string JSON_EXTENSION = ".json";
        public const string JSON_START_ARRAY = "[";
        public const string JSON_END_ARRAY = "]";
    }
}
﻿using System;

namespace FilesAPI.Core.Models.Base
{
    public class LifeTimeBase
    {
        public LifeTimeBase()
        {
            DateCreated = DateTime.UtcNow;
            DateUpdated = DateTime.UtcNow;
            DateDeleted = null;
        }

        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime? DateDeleted { get; set; }
    }
}
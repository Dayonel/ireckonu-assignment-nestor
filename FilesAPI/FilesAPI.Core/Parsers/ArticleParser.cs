﻿using FilesAPI.Core.DTOs;
using FilesAPI.Core.Interfaces.Parsers;

namespace FilesAPI.Core.Parsers
{
    public class ArticleParser : IArticleParser
    {
        public ArticleDTO Parse(string line)
        {
            if (string.IsNullOrEmpty(line))
                return new ArticleDTO();

            string[] fields = line.Split(CSVConstants.CSV_SEPARATOR);
            return new ArticleDTO()
            {
                Key = fields[0],
                ArtikelCode = fields[1],
                ColorCode = fields[2],
                Description = fields[3],
                Price = int.Parse(fields[4]),
                DiscountPrice = int.Parse(fields[5]),
                DeliveredIn = fields[6],
                Q1 = fields[7],
                Size = int.Parse(fields[8]),
                Color = fields[9]
            };
        }
    }
}
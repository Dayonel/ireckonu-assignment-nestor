﻿namespace FilesAPI.Core
{
    public static class PathConstants
    {
        public const string UPLOADED_FILES_FOLDER = "wwwroot/UploadedFiles";
        public const string REQUEST_PATH_URL = "/uploaded-files";
    }
}
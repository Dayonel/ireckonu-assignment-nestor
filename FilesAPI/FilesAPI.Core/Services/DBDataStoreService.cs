﻿using System.Threading.Tasks;
using AutoMapper;
using FilesAPI.Core.DTOs;
using FilesAPI.Core.Interfaces.Repositories;
using FilesAPI.Core.Interfaces.Services;
using FilesAPI.Core.Models;

namespace FilesAPI.Core.Services
{
    public class DBDataStoreService : IDBDataStoreService
    {
        private readonly IMapper _mapper;
        private readonly IArticleRepository _articleRepository;
        public DBDataStoreService(IMapper mapper, IArticleRepository articleRepository)
        {
            _mapper = mapper;
            _articleRepository = articleRepository;
        }

        public async Task<bool> AddArticle(ArticleDTO articleDTO)
        {
            Article article = _mapper.Map<Article>(articleDTO);
            return await _articleRepository.AddArticle(article);
        }
    }
}
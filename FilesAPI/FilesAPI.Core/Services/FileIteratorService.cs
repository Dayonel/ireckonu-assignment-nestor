﻿using FilesAPI.Core.DTOs;
using FilesAPI.Core.Interfaces.Services;
using System.Collections.Generic;
using System.IO;

namespace FilesAPI.Core.Services
{
    public class FileIteratorService : IFileIteratorService
    {
        public async IAsyncEnumerable<FileLineDTO> IterateLines(Stream stream, bool skipFirstLine = false)
        {
            using (StreamReader sr = new StreamReader(stream))
            {
                // 1. Set reader position
                sr.BaseStream.Position = 0;

                // 2. Check skip first line
                if (skipFirstLine)
                    await sr.ReadLineAsync();

                // 3. Read the file
                while (!sr.EndOfStream)
                {
                    yield return new FileLineDTO() { LineText = await sr.ReadLineAsync(), IsLastLine = sr.EndOfStream };
                }

                sr.Close();
            }

            yield break;
        }
    }
}
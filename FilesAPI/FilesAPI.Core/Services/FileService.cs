﻿using System.IO;
using System.Threading.Tasks;
using FilesAPI.Core.DTOs;
using FilesAPI.Core.Interfaces.Generators;
using FilesAPI.Core.Interfaces.Parsers;
using FilesAPI.Core.Interfaces.Services;

namespace FilesAPI.Core.Services
{
    public class FileService : IFileService
    {
        private readonly IArticleParser _articleParser;
        private readonly IFileIteratorService _fileIteratorService;
        private readonly IDBDataStoreService _dBDataStoreService;
        private readonly IJSONDataStoreService _jSONDataStoreService;
        private readonly IFilePathGenerator _filePathGenerator;
        public FileService(IArticleParser articleParser, IFileIteratorService fileIteratorService, IDBDataStoreService dBDataStoreService,
                           IJSONDataStoreService jSONDataStoreService, IFilePathGenerator filePathGenerator)
        {
            _articleParser = articleParser;
            _fileIteratorService = fileIteratorService;
            _dBDataStoreService = dBDataStoreService;
            _jSONDataStoreService = jSONDataStoreService;
            _filePathGenerator = filePathGenerator;
        }

        public async Task<bool> UploadFile(Stream stream)
        {
            string filePath = _filePathGenerator.GenerateFilePath();
            if (!await _jSONDataStoreService.Write(filePath, JSONConstants.JSON_START_ARRAY))
                return false;

            await foreach (var line in _fileIteratorService.IterateLines(stream, true))
            {
                ArticleDTO articleDTO = _articleParser.Parse(line.LineText);
                if (!await _dBDataStoreService.AddArticle(articleDTO))
                    return false;

                articleDTO.SetFileLine(filePath, line.LineText, line.IsLastLine);
                if (!await _jSONDataStoreService.AddArticle(articleDTO))
                    return false;
            }

            if (!await _jSONDataStoreService.Write(filePath, JSONConstants.JSON_END_ARRAY))
                return false;

            return true;
        }
    }
}
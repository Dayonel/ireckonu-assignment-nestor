﻿using System.Threading.Tasks;
using FilesAPI.Core.DTOs;
using FilesAPI.Core.Interfaces.PhysicalStorage;
using FilesAPI.Core.Interfaces.Services;

namespace FilesAPI.Core.Services
{
    public class JSONDataStoreService : IJSONDataStoreService
    {
        private readonly IPhysicalArticleStorage _physicalArticleStorage;
        private readonly IJSONFormatterService _jSONFormatterService;
        public JSONDataStoreService(IPhysicalArticleStorage physicalArticleStorage, IJSONFormatterService jSONFormatterService)
        {
            _physicalArticleStorage = physicalArticleStorage;
            _jSONFormatterService = jSONFormatterService;
        }

        public async Task<bool> AddArticle(ArticleDTO articleDTO)
        {
            var json = _jSONFormatterService.SerializeToJSON(articleDTO);
            if (!await _physicalArticleStorage.WriteLine(articleDTO.FilePath, json))
                return false;

            if (!articleDTO.IsLastLine)
                return await _physicalArticleStorage.Write(articleDTO.FilePath, JSONConstants.JSON_SEPARATOR);

            return true;
        }

        public async Task<bool> Write(string filePath, string text)
        {
            return await _physicalArticleStorage.Write(filePath, text);
        }

        public async Task<bool> WriteLine(string filePath, string line)
        {
            return await _physicalArticleStorage.WriteLine(filePath, line);
        }
    }
}
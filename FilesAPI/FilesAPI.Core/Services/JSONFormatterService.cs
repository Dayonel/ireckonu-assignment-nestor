﻿using FilesAPI.Core.Interfaces.Services;
using Newtonsoft.Json;

namespace FilesAPI.Core.Services
{
    public class JSONFormatterService : IJSONFormatterService
    {
        public string SerializeToJSON<T>(T input)
        {
            return JsonConvert.SerializeObject(input);
        }
    }
}
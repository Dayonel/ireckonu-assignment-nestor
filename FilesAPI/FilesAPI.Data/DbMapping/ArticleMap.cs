﻿using FilesAPI.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace FilesAPI.Data.DbMapping
{
    public static class ArticleMap
    {
        public static ModelBuilder MapArticle(this ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Article>();

            // Primary Key
            entity.HasKey(t => t.Id);

            return modelBuilder;
        }
    }
}
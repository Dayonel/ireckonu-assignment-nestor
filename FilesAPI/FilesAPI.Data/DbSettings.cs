﻿using FilesAPI.Core.Interfaces.Settings;

namespace FilesAPI.Data
{
    public class DbSettings : IDbSettings
    {
        public string ConnectionString { get; set; }
    }
}
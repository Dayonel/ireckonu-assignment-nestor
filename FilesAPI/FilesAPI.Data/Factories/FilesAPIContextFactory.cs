﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace FilesAPI.Data.Factories
{
    public class FilesAPIContextFactory : IDesignTimeDbContextFactory<FilesAPIContext>
    {
        public FilesAPIContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<FilesAPIContext>();
#if DEBUG
            builder.UseSqlServer("Server=localhost;Database=FilesAPI-Test;Trusted_Connection=True;",
            opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds));
#else
           builder.UseSqlServer("Server=tcp:localhost,1433;Database=FilesAPI;User Id=root;Password=5lTsw!x_--un#ay2xUSta0&lSw1c?lhlcuX!*%m7tuTid+ek$atrAwrerAs02$Ri;",
           opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(5).TotalSeconds));
#endif
            return new FilesAPIContext(builder.Options);
        }
    }
}
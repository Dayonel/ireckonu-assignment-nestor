﻿using FilesAPI.Core.Models;
using FilesAPI.Data.DbMapping;
using FilesAPI.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FilesAPI.Data
{
    public class FilesAPIContext : DbContext, IFilesAPIContext
    {
        public FilesAPIContext(DbContextOptions options) : base(options) { }

        public DbSet<Article> Articles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.MapArticle();
        }
    }
}
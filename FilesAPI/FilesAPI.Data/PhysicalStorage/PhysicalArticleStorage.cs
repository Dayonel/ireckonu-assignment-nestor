﻿using System;
using System.IO;
using System.Threading.Tasks;
using FilesAPI.Core.Interfaces.PhysicalStorage;
using Microsoft.Extensions.Logging;

namespace FilesAPI.Data.Storage
{
    public class PhysicalArticleStorage : IPhysicalArticleStorage
    {
        private readonly ILogger<PhysicalArticleStorage> _logger;
        public PhysicalArticleStorage(ILogger<PhysicalArticleStorage> logger)
        {
            _logger = logger;
        }

        public async Task<bool> Write(string filePath, string text)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    await sw.WriteAsync(text);
                    sw.Close();
                    fs.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
                return false;
            }
        }

        public async Task<bool> WriteLine(string filePath, string line)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    await sw.WriteLineAsync(line);
                    sw.Close();
                    fs.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
                return false;
            }
        }
    }
}
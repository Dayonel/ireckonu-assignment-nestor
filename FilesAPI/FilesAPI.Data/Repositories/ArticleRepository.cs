﻿using System;
using System.Threading.Tasks;
using FilesAPI.Core.Interfaces.Repositories;
using FilesAPI.Core.Models;
using FilesAPI.Data.Interfaces;
using Microsoft.Extensions.Logging;

namespace FilesAPI.Data.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly IFilesAPIContext _dbContext;
        private readonly ILogger<ArticleRepository> _logger;
        public ArticleRepository(IFilesAPIContext dbContext, ILogger<ArticleRepository> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<bool> AddArticle(Article article)
        {
            try
            {
                _dbContext.Articles.Add(article);
                return await _dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex.ToString());
                return false;
            }
        }
    }
}
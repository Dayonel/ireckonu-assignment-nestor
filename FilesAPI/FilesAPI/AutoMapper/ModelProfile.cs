﻿using AutoMapper;
using FilesAPI.Core.DTOs;
using FilesAPI.Core.Models;

namespace FilesAPI.AutoMapper
{
    public class ModelProfile : Profile
    {
        public ModelProfile()
        {
            CreateMap<ArticleDTO, Article>()
                .ForMember(dst => dst.Id, opt => opt.Ignore())
                .ForMember(dst => dst.DateCreated, opt => opt.Ignore())
                .ForMember(dst => dst.DateUpdated, opt => opt.Ignore())
                .ForMember(dst => dst.DateDeleted, opt => opt.Ignore());
        }
    }
}
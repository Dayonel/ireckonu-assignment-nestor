﻿using AutoMapper;
using FilesAPI.Core.DTOs;
using FilesAPI.Core.Models;

namespace FilesAPI.AutoMapper
{
    public class ObjectProfile : Profile
    {
        public ObjectProfile()
        {
            CreateMap<Article, ArticleDTO>()
                .ForMember(dst => dst.FilePath, opt => opt.Ignore());
        }
    }
}
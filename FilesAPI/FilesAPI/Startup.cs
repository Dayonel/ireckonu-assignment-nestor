using AutoMapper;
using FilesAPI.AutoMapper;
using FilesAPI.Core;
using FilesAPI.Core.Generators;
using FilesAPI.Core.Interfaces.Generators;
using FilesAPI.Core.Interfaces.Parsers;
using FilesAPI.Core.Interfaces.PhysicalStorage;
using FilesAPI.Core.Interfaces.Repositories;
using FilesAPI.Core.Interfaces.Services;
using FilesAPI.Core.Interfaces.Settings;
using FilesAPI.Core.Parsers;
using FilesAPI.Core.Services;
using FilesAPI.Data;
using FilesAPI.Data.Interfaces;
using FilesAPI.Data.Repositories;
using FilesAPI.Data.Storage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.IO;

namespace FilesAPI
{
    public class Startup
    {
        public Startup(IWebHostEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddJsonFile($"connectionstrings{(env.IsDevelopment() ? "_development" : "")}.json", false, true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region DI
            services.AddSingleton<IDbSettings>(Configuration.BindSettings<DbSettings>(nameof(FilesAPIContext)));
            services.AddDbContext<FilesAPIContext>(options => options.UseSqlServer(Configuration.BindSettings<DbSettings>(nameof(FilesAPIContext)).ConnectionString));
            services.AddScoped<IFilesAPIContext, FilesAPIContext>();

            services.AddScoped<IFilePathGenerator, FilePathGenerator>();

            // Services
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IFileIteratorService, FileIteratorService>();
            services.AddScoped<IDBDataStoreService, DBDataStoreService>();
            services.AddScoped<IJSONDataStoreService, JSONDataStoreService>();
            services.AddScoped<IJSONFormatterService, JSONFormatterService>();

            // Repositories
            services.AddScoped<IArticleRepository, ArticleRepository>();
            services.AddScoped<IPhysicalArticleStorage, PhysicalArticleStorage>();

            // Parsers
            services.AddScoped<IArticleParser, ArticleParser>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ObjectProfile());
                mc.AddProfile(new ModelProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            #endregion

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Files API", Version = "v1" });
            });

            services.AddMvc();
            services.Configure<FormOptions>(x =>
            {
                x.MultipartBodyLengthLimit = long.MaxValue;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Files API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseStaticFiles(); // For the wwwroot folder

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), PathConstants.UPLOADED_FILES_FOLDER)),
                RequestPath = new PathString(PathConstants.REQUEST_PATH_URL)
            });

            app.UseDirectoryBrowser(new DirectoryBrowserOptions()
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), PathConstants.UPLOADED_FILES_FOLDER)),
                RequestPath = new PathString(PathConstants.REQUEST_PATH_URL)
            });
        }
    }
}
# Files API
Files API can be found in live here:
https://www.filesapi-staging.com/swagger/index.html
![N|Solid](https://i.imgur.com/XgfRBIt.png)

### 1. Install MS SQL Server
https://go.microsoft.com/fwlink/?linkid=853016

### 2. Install .NET Core SDK 3.1.101
https://dotnet.microsoft.com/download/dotnet-core/thank-you/sdk-3.1.101-windows-x64-installer

### 3. Execute migration
- Compile the solution in Debug mode
- Select as "Startup project" the Console Application called FilesAPI.Migrator under the Utils folder.
![N|Solid](https://i.imgur.com/bvdh0PZ.png)
- Open the Packet Manager console from Tools -> Nuget package admin console and **select FilesAPI.Data** as default project.
![N|Solid](https://i.imgur.com/7WZFr1h.png)
- Write the following command to recreate the empty database: ```Update-Database```
![N|Solid](https://i.imgur.com/ttqeCf2.png)
A database should have been created with empty Articles table.
![N|Solid](https://i.imgur.com/wmo4u02.png)

### 4. Run project
- Select FilesAPI as "Startup project".
![N|Solid](https://i.imgur.com/xMgZR1S.png)
- Change the execution mode from **IIS Express** to **FilesAPI** (run in console)
![N|Solid](https://i.imgur.com/gSAkDFZ.png)
- Start API with **F5**
- The Swagger UI should be shown, if not, navigate to this url: 
http://localhost:**YOUR_PORT**/swagger/index.html
- Expand the POST method row by clicking on it and click in **Try it out**
![N|Solid](https://i.imgur.com/vUawKuN.png)
- Select the desired CSV file by clicking on **Browse** and select the file from your computer. Then click in **Execute** button.
![N|Solid](https://i.imgur.com/umbQ51z.png)
- If the file has been saved and inserted into DB, **true** will be returned in the response, otherwise **false**.
- Check the Articles table in database
![N|Solid](https://i.imgur.com/D5Wtfu8.png)
- Navigate to: http://localhost:**YOUR_PORT**/uploaded-files/
The JSON file should be shown:
![N|Solid](https://i.imgur.com/3asqFMH.png)

### 5. Production database
The staging database can be accessed after uploading files to check the rows with the following credentials:
HOST: ``` 128.199.60.124 ```
User: ``` sa ```
Password: ``` AR4_F558-N335ZVvXkrP_8 ```
![N|Solid](https://i.imgur.com/8vs9F4I.png)